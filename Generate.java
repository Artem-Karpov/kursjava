package schedule;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Scanner;

public class Generate
{
    public Generate()
    {}

    public void generate()
    {
        int numberOfShips = (int)(Math.random() * 101.0D) + 1;
        for(int i = 0; i < numberOfShips; i++)
        {
            String name = "Ship " + (int)(Math.random() * 101.0D);
            int date = (int)(Math.random() * 30);
            CargoType cargoType = CargoType.LOOSE;
            int random = (int) (Math.random() * 2);
            switch(random)
            {
                case 1:
                    cargoType = CargoType.LOOSE;
                    break;
                case 2:
                    cargoType = CargoType.LIQUID;
                    break;
                case 3:
                    cargoType = CargoType.CONTAINER;
            }
            int cargoWeight = (int)(Math.random() * 300000.0D);
            Time time = new Time();
            time.setHours((int)(Math.random() * 24));
            time.setMinutes((int)(Math.random() * 60));
            new Ship(name, cargoType, cargoWeight, date, time);
        }
    }

    public void ManualEntry()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the ship number: ");
        Ship ship = new Ship();
        Time time = new Time();
        int name;
        if (in.hasNextInt())
        {
            name = in.nextInt();
            ship.setName(name);
        }

        int cargoType;
        System.out.println("Enter type of cargo 1)LOOSE 2)LIQUID 3)CONTAINER : ");
        if (in.hasNextInt())
        {
            cargoType = in.nextInt();
            ship.setCargoType(cargoType);
        }

        int cargoWeight;
        System.out.println("Enter weight of cargo: ");
        if (in.hasNextInt())
        {
            cargoWeight = in.nextInt();
            ship.setCargoWeight(cargoWeight);
        }

        int date;
        System.out.println("Enter arrival day: ");
        if (in.hasNextInt())
        {
            date = in.nextInt();
            ship.setDate(date);
        }

        int hours;
        System.out.println("Enter arrival hours: ");
        if (in.hasNextInt())
        {
            hours = in.nextInt();
            time.setHours(hours);
        }

        int minutes;
        System.out.println("Enter arrival minutes: ");
        if (in.hasNextInt())
        {
            minutes = in.nextInt();
            time.setMinutes(minutes);
        }

        ship.setTime(time);
        ship.setUnloadTime();
    }
}
